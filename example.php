<?php

$pdo = new PDO('pgsql:dbname=mibaseDatos;host=localhost', 'username', 'password');

include_once('Session/BluSession.php');

$config['database'] = $pdo;

$session = new BluSession($config);    

if (!isset($_SESSION['isLogged'])){    
    print_r("redirigir al login");
    die();
}

$session->setData('someData', 'someValue');

print_r($_SESSION);
die();


?>
