<?php

require_once("SessionDriver.php");

class SessionDatabaseDriver extends SessionDriver implements SessionHandlerInterface {
    
    protected $_db = NULL;    
    protected $_rowExists = FALSE;
    
    public function __construct($config) {        
        parent::__construct($config);
        
        if (isset($config['database'])){
            $this->_db = $config['database'];
        }
    }   
    
    public function open ($savePath, $sessionName){
        if ($this->_db === NULL){
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function close (){        
        return true;
    }
    
    public function read ($sessionId){
        $this->_sessionId = $sessionId;
        
        $stmt = $this->_db->prepare("SELECT data FROM {$this->_config['tableName']} WHERE time_updated >= ? AND id=?");        
        $stmt->execute(array(time(), $this->_expressMutator($sessionId)));
        
        if (!$stmt){
            print_r($stmt->errorInfo());
            die();
        }

        $result = $stmt->fetch(PDO::FETCH_ASSOC);                
        
        $result = json_decode(base64_decode(rtrim($result['data'])), TRUE);
        
        if ($result !== FALSE && count($result) > 0){
            $this->_rowExists = TRUE;
            return $this->_serializeSession($result);
        }
        
        $this->_rowExists = FALSE;
        return '';        
    }
    
    public function write($sessionId, $sessionData) {        
        $sessionData = $this->_unserializeSession($sessionData);        
        $sessionData['time_updated'] = time();        

        $sessionData = base64_encode(json_encode($sessionData));

        if (isset($this->_sessionId) && $sessionId !== $this->_sessionId){
            $this->_sessionId = $sessionId;
            $this->_rowExists = FALSE;            
        }
        
        if ($this->_rowExists === FALSE){            
            $stmt = $this->_db->prepare("INSERT INTO {$this->_config['tableName']} (id, ip_address, time_updated, data) VALUES (?, ?, ?, ?)");            
            
            $stmt->execute(array($this->_expressMutator($sessionId), $_SERVER['REMOTE_ADDR'], (time() + $this->_config['cookieLifetime']), $sessionData));
            
            if (!$stmt){
                print_r($stmt->errorInfo());
                die();
            }
            
            return TRUE;
        }else{
            $stmt = $this->_db->prepare("UPDATE {$this->_config['tableName']} SET data=?, time_updated=? WHERE id=?");
            
            $stmt->execute(array($sessionData, (time() + $this->_config['cookieLifetime']), $this->_expressMutator($sessionId)));
            
            if (!$stmt){
                print_r($stmt->errorInfo());
                die();
            }
            return TRUE;
        }        
        return FALSE;
    }

    public function destroy($sessionId) {        
        $stmt = $this->_db->prepare("DELETE FROM {$this->_config['tableName']} WHERE id=?");
        $stmt->execute(array( $this->_expressMutator($sessionId)));
        
        if (!$stmt){
            print_r($stmt->errorInfo());
            die();
        }        
        
        $this->_cookieDestroy();
        return TRUE;
    }
    
    public function gc ($lifetime){
        $stmt = $this->_db->prepare("DELETE FROM {$this->_config['tableName']} WHERE time_updated < ?");
        $stmt->execute(array(time()));
        
        if (!$stmt){
            print_r($stmt->errorInfo());
            die();
        }
        return true;
    } 
}

?>
