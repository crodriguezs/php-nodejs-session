<?php

require_once("SessionDatabaseDriver.php");

class BluSession {
    
    protected $_config = array(
        'database' => NULL,
        'cookieName' => 'BLUSESSION',
        'cookieDomain' => '.blutest.tk',
        'tableName' => 'master.sessions',
        'cookieSecure' => FALSE,
        'cookieLifetime' => 86400,
        'cookiePath' => '/'        
    );            
    
    private $_sessSecret = 'ET phone home';
    private $_sid_regexp;
        
    public function __construct($params){
        $this->_setConfig($params);
        
        $class = new SessionDatabaseDriver($this->_config);
        
        session_set_save_handler(
            array($class, 'open'),
            array($class, 'close'),
            array($class, 'read'),
            array($class, 'write'),
            array($class, 'destroy'),
            array($class, 'gc')            
        );
        
        register_shutdown_function('session_write_close');
        
        $this->_initSession();
    }
        
    private function _initSession(){
        $sessName = session_name();
        session_start();
        
        if (isset($_COOKIE[$sessName])){            
            $this->_validateCookie($sessName);
        }else {            
            $this->_generateId();
            $this->_setCookie();
        }    
    }
    
    public function getData($key){
        if (isset($key)) {
            return isset($_SESSION[$key]) ? $_SESSION[$key] : NULL;
        }
    }
    
    public function setData($key, $value){
        if (isset($key)){
            $_SESSION[$key] = $value;
        }        
    }
    
    public function unsetData($key){
        if (isset($_SESSION[$key])) unset($SESSION[$key]);
    }
    
    public function getAllData(){
        return $_SESSION;
    }
                    
    private function _validateCookie($sessName) {               
        $this->_sessionId = $_COOKIE[$sessName];        
        
        $sessionId = $this->_sessionId;
        
        if (substr($sessionId, 0, 2) === "s:")
            $sessionId = substr($sessionId, 2);
        
        $dotPosition = strpos($sessionId, ".");
        if ($dotPosition !== FALSE){
            $hashOnCookie = substr($sessionId, $dotPosition + 1);
            $sessionId = substr($sessionId, 0, $dotPosition);
            
            $hashCalc = str_replace("=", "", base64_encode(hash_hmac('sha256', $sessionId, $this->_sessSecret, true)));
            if ($hashCalc !== $hashOnCookie) {
                // destroy fucking cookies spoofing
                $this->_cookieDestroy();
            }
        }
    }
    
    private function _setCookie(){
        setCookie(
            $this->_config['cookieName'],
            $this->_sessionId,
            time() + $this->_config['cookieLifetime'],
            $this->_config['cookiePath'],
            $this->_config['cookieDomain'],
            $this->_config['cookieSecure'], // HTTPS
            TRUE // httpOnly
        );
    }    
    
    private function _generateId(){
        $_SESSION['time_updated'] = time();
        session_regenerate_id(FALSE);
        
        $sessionId = session_id();
        $hash = str_replace("=", "", base64_encode(hash_hmac('sha256', $sessionId, $this->_sessSecret, true)));
        $this->_sessionId = "s:$sessionId.$hash";        
        session_id($this->_sessionId);        
    }   
               
    private function _setConfig($params){
        if (isset($params['database'])){
            $this->_config['database'] = $params['database'];
        }else{
            throw new Exception('Database handler not set! :( Require PDO Object... please!');
        }
        
        if (isset($params['cookieName'])){
            if (! ctype_alnum(str_replace(array('-', '_'), '', $params['cookieName']))) {
                throw new Exception('woopsis! invalid cookie name!');
            }            
            $this->_config['cookieName'] = $params['cookieName'];
        }

        if (isset($params['cookieDomain'])){
            if (! ctype_alnum(str_replace(array('-', '_'), '', $params['cookieDomain']))) {
                throw new Exception('woopsis! invalid cookie domain!');
            }
            
            $this->_config['cookieDomain'] = $params['cookieDomain'];
        }
        
        if (isset($params['tableName'])){            
            if (! ctype_alnum(str_replace(array('-', '_'), '', $params['tableName']))) {
                throw new Exception('daah! invalid table name!');
            }
            
            $this->_config['tableName'] = $params['tableName'];
        }
        
        if (isset($params['cookieSecure'])){
            if (! is_bool($params['cookieSecure'])) {
                throw new Exception('The secure cookie option must be either TRUE or FALSE.');
            }
            
            $this->_config['cookieSecure'] = $params['cookieSecure'];
        }


        if (isset($params['cookieLifetime'])){            
            if (! is_int($params['cookieLifetime']) || ! preg_match('#[0-9]#', $params['cookieLifetime'])) {
                throw new Exception('Seconds till expiration must be a valid number.');
            }
            
            if ($params['cookieLifetime'] < 1) {
                throw new Exception('Seconds till expiration can not be zero or less. Enable session expiration when the browser closes instead.');
            }
            
            $this->_config['cookieLifetime'] = (int) $params['cookieLifetime'];
        }
        
        ini_set('session.name', $this->_config['cookieName']);
        ini_set('session.cookie_domain', $this->_config['cookieDomain']);
        ini_set('session.save_path', $this->_config['cookiePath']);
        ini_set('session.hash_function', 1);


        // security
        ini_set('session.use_trans_sid', 0);
        ini_set('session.use_strict_mode', 1);
        ini_set('session.use_cookies', 1);
        ini_set('session.use_only_cookies', 1);

        $this->_configure_sid_length();
    }

    
    protected function _configure_sid_length(){
        if (PHP_VERSION_ID < 70100){
            $hash_function = ini_get('session.hash_function');
            if (ctype_digit($hash_function)){
                if ($hash_function !== '1'){
                    ini_set('session.hash_function', 1);
                }
                
                $bits = 160;
            }elseif ( ! in_array($hash_function, hash_algos(), TRUE)){
                ini_set('session.hash_function', 1);
                $bits = 160;
            }
            elseif (($bits = strlen(hash($hash_function, 'dummy', false)) * 4) < 160){
                ini_set('session.hash_function', 1);
                $bits = 160;
            }
            
            $bits_per_character = (int) ini_get('session.hash_bits_per_character');
            $sid_length         = (int) ceil($bits / $bits_per_character);
        }else {
            $bits_per_character = (int) ini_get('session.sid_bits_per_character');
            $sid_length         = (int) ini_get('session.sid_length');
            if (($bits = $sid_length * $bits_per_character) < 160){
                // Add as many more characters as necessary to reach at least 160 bits_per_character$sid_length += (int) ceil((160 % $bits) / $bits_per_character);
                ini_set('session.sid_length', $sid_length);
            }
        }
        
        // Yes, 4,5,6 are the only known possible values as of 2016-10-27
        switch ($bits_per_character){
        case 4:
            $this->_sid_regexp = '[0-9a-f]';
            break;
        case 5:
            $this->_sid_regexp = '[0-9a-v]';
            break;
        case 6:
            $this->_sid_regexp = '[0-9a-zA-Z,-]';
            break;
        }
        
        $this->_sid_regexp .= '{'.$sid_length.'}';
    }

}
?>
