<?php

abstract class SessionDriver implements SessionHandlerInterface {
    
    protected $_config;
    protected $_sessionId;
    
    public function __construct($config){
        $this->_config = $config;        
    }
    
    protected function _cookieDestroy() {
        return setcookie(
            $this->_config['cookieName'],
            NULL,
            1,
            $this->_config['cookiePath'],
            $this->_config['cookieDomain'],
            $this->_config['cookieSecure'],
            TRUE
        );
    }
    
    // convert nodejs encoded session_id to default format
    protected function _expressMutator($sessionId){
        if (substr($sessionId, 0, 2) === "s:")
            $sessionId = substr($sessionId, 2);
        
        $dotPosition = strpos($sessionId, ".");
        if ($dotPosition !== FALSE){
            $hashOnCookie = substr($sessionId, $dotPosition + 1);
            $sessionId = substr($sessionId, 0, $dotPosition);
        }        
        return $sessionId;        
    }
    
    protected function _serializeSession(array $array) {
        $raw = '';
        $line = 0;
        $keys = array_keys($array);
        foreach ($keys as $key) {
            $value = $array[$key];
            $line++;
            $raw .= $key . '|';
            if (is_array($value) && isset($value['huge_recursion_blocker_we_hope'])) {
                $raw .= 'R:' . $value['huge_recursion_blocker_we_hope'] . ';';
            } else {
                $raw .= serialize($value);
            }
            $array[$key] = Array('huge_recursion_blocker_we_hope' => $line);
        }
        return $raw;
    }
        
    protected function _unserializeSession($sessionData) {
        $returnData = array();
        $offset = 0;
        while ($offset < strlen($sessionData)) {
            if (!strstr(substr($sessionData, $offset), "|")) {
                throw new Exception("invalid data, remaining: " . substr($sessionData, $offset));
            }
            $pos = strpos($sessionData, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($sessionData, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($sessionData, $offset));
            $returnData[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $returnData;
    }    
}

?>
